--[[
NerfedEngine acts as the core of the addon keeping track
of what effects and abilities are on you and your targets,
the position of your character and any virtual cooldowns.
]]--

-- ==========================
-- ===== Public members =====
-- ==========================

NerfedEngine = { 
    BARSLOT4TEST    = 118, -- bar slot used for cooldown tests
    
    TIME_DELAY      = .2,

    Loaded          = false,
    
    RunepriestZealotMechanic = false
}


-- ===========================
-- ===== Private members =====
-- ===========================
local GlobalCooldown  = 0

local AbilityCasting  = nil
local CastTime        = 0 -- Needed until game v1.2

local TimeLeft = NerfedEngine.TIME_DELAY


-- ===========================
-- ===== Private methods =====
-- ===========================

-- Shortcut function
local function changeHotbar(barSlot, id)
    if NerfedAPI.IsAbility(id) then
        SetHotbarData(barSlot, GameData.PlayerActions.DO_ABILITY, id)
    elseif NerfedAPI.IsItem(id) then
        SetHotbarData(barSlot, GameData.PlayerActions.USE_ITEM, id)
    else
        d("NerfedEngine:changeHotbar:Invalid type of id")
    end
end

-- ============================
-- ===== Internal methods =====
-- ============================
    
function NerfedEngine.Initialize()
    -- register event handler
    RegisterEventHandler(SystemData.Events.PLAYER_BEGIN_CAST, "NerfedEngine.OnPlayerBeginCast" )
    RegisterEventHandler(SystemData.Events.PLAYER_END_CAST, "NerfedEngine.OnPlayerEndCast" )
    RegisterEventHandler(SystemData.Events.WORLD_OBJ_COMBAT_EVENT, "NerfedEngine.OnCombatEvent")
    RegisterEventHandler(SystemData.Events.PLAYER_TARGET_UPDATED, "NerfedEngine.OnTargetUpdated")
    RegisterEventHandler(SystemData.Events.PLAYER_ABILITY_TOGGLED, "NerfedEngine.OnAbilityToggled" )
    RegisterEventHandler(SystemData.Events.PLAYER_CAST_TIMER_SETBACK, "NerfedEngine.OnSetbackCast")
    -- RegisterEventHandler(SystemData.Events.PLAYER_POSITION_UPDATED, "NerfedEngine.OnMove")
    RegisterEventHandler(SystemData.Events.LOADING_END, "NerfedEngine.OnLoading")
    RegisterEventHandler(SystemData.Events.RELOAD_INTERFACE, "NerfedEngine.OnLoading")

    NerfedEngine.Loaded = true
    return NerfedEngine.Loaded
end

function NerfedEngine.OnLoading()
	if ActionBars.m_ActiveActions[1659] == true or ActionBars.m_ActiveActions[8550] == true then 
		RunepriestZealotMechanic = 1 
	else 
		RunepriestZealotMechanic = 0 
	end
end

function NerfedEngine.OnUpdate(elapsed)
    if not NerfedEngine.Loaded or not NerfedMemory.Loaded then
        return
    end

    -- Abilities cast time
    if CastTime > 0 then
        CastTime = CastTime - elapsed
        if CastTime <= 0 then
            CastTime = 0
            AbilityCasting = nil
            NerfedButtons.NeedUpdate = true
        end
    end
    
    -- update global cooldown
    if GlobalCooldown > 0 then
        GlobalCooldown = GlobalCooldown - elapsed
        if GlobalCooldown < 0 then
            GlobalCooldown = 0
        end
    end

    -- stop the code from eating up all CPU resources
    TimeLeft = TimeLeft - elapsed
    if TimeLeft > 0 then
        return -- cut out early
    end

    -- reset to TIME_DELAY seconds
    TimeLeft = NerfedEngine.TIME_DELAY
    

end

--
function NerfedEngine.OnPlayerBeginCast(abilityId, isChannel, desiredCastTime, averageLatency)
    -- Current casting ability
    if (desiredCastTime > 0) then
        AbilityCasting = abilityId
        if NerfedMemory.GetFullStayOnCast() then
            CastTime = desiredCastTime + averageLatency
        else
            CastTime = 0.5
        end
    end
    
    -- Global CoolDown
    GlobalCooldown = 1.5
    
    -- update all buttons
    NerfedButtons.NeedUpdate = true
end


--
function NerfedEngine.OnSetbackCast(newCastTime)
    if NerfedMemory.GetFullStayOnCast() and CastTime ~= 0 then
        CastTime = newCastTime
    end
end


--
function NerfedEngine.OnPlayerEndCast(interupt)

    AbilityCasting = nil
    CastTime = 0
    
    -- update all buttons
    NerfedButtons.NeedUpdate = true
end

--
function NerfedEngine.OnCombatEvent(targetObjNum, eventMagnitude, eventType, abilityId)
    if abilityId ~= 0 and
	      (eventType == GameData.CombatEvent.BLOCK   or eventType == GameData.CombatEvent.PARRY   or
	       eventType == GameData.CombatEvent.EVADE   or eventType == GameData.CombatEvent.DISRUPT or
	       eventType == GameData.CombatEvent.ABSORB  or eventType == GameData.CombatEvent.IMMUNE) then

        -- update all buttons
        NerfedButtons.NeedUpdate = true
    end
end

--
function NerfedEngine.OnAbilityToggled(abilityId, toggledOn)
	-- Runepriest and Zealot mechanic catch (may be a better way to do this...)
	if abilityId == 1659 or abilityId == 8550 then
		if toggledOn == true then
			RunepriestZealotMechanic = 1
		else
			RunepriestZealotMechanic = 0
		end
	end

    -- update all buttons
    NerfedButtons.NeedUpdate = true
end

local lastTarget = 0
--
function NerfedEngine.OnTargetUpdated(targetClassification, targetId, targetType)
    if targetClassification == TargetInfo.FRIENDLY_TARGET and targetId ~= lastTarget then
        TimeLeft = 0
        NerfedButtons.NeedUpdate = true
        lastTarget = targetId
    end
end


-- ==========================
-- ===== Public methods =====
-- ==========================

function NerfedEngine.AbilityBeingCast()
    return AbilityCasting
end

-- returns whether an action is on cooldown
-- achieves this by placing the action on a hidden hotbarslot
function NerfedEngine.OnCooldown(actionId)
    changeHotbar(NerfedEngine.BARSLOT4TEST, actionId)
    local cooldown = GetHotbarCooldown(NerfedEngine.BARSLOT4TEST)
    -- update action data with cooldown information
    local actionData = NerfedMemory.getActionDataCache(actionId)
    actionData.cooldown = cooldown
    NerfedMemory.setActionDataCache(actionId, actionData)
    return cooldown and (cooldown > GlobalCooldown + 0.1)
end



-- RunepriestZealotMechanic hack
function NerfedEngine.getRunepriestZealotMechanic()
	return RunepriestZealotMechanic
end
